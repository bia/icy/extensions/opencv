package plugins.adufour.opencv;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JSeparator;

import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import icy.image.IcyBufferedImage;
import icy.main.Icy;
import icy.math.FPSMeter;
import icy.sequence.Sequence;
import icy.sequence.SequenceAdapter;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVar;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarListener;
import plugins.adufour.vars.util.VarException;

public class OpenCVCapture extends EzPlug implements EzStoppable, EzVarListener<Integer>
{
    List<EzVarInteger> parameters = new ArrayList<EzVarInteger>();
    
    VideoCapture camera = null;
    
    @Override
    protected void initialize()
    {
        OpenCV.initialize();
        
        for (Field f : Videoio.class.getDeclaredFields())
        {
            String name = f.getName();
            if (!name.startsWith("CAP_PROP_")) continue;
            if (name.contains("_DC1394")) continue;
            if (name.contains("_GIGA")) continue;
            if (name.contains("_GPHOTO2")) continue;
            if (name.contains("_GSTREAMER")) continue;
            if (name.contains("_INTELPERC")) continue;
            if (name.contains("_IOS")) continue;
            if (name.contains("_OPENNI")) continue;
            if (name.contains("_POS")) continue;
            if (name.contains("_PVAPI")) continue;
            if (name.contains("_XI")) continue;
            
            name = name.substring(9).toLowerCase();
            EzVarInteger setting = new EzVarInteger(name);
            setting.setValue(Integer.valueOf(-1));
            setting.addVarChangeListener(this);
            parameters.add(setting);
        }
        
        int cpt = 0;
        for (EzVarInteger setting : parameters)
        {
            addEzComponent(setting);
            cpt++;
            if (cpt % 20 == 0) addComponent(new JSeparator(JSeparator.VERTICAL));
        }
    }
    
    @Override
    protected void execute()
    {
        camera = new VideoCapture(0);
        
        for (EzVarInteger setting : parameters)
        {
            if (setting.getValue() == -1) continue;
            
            String propName = "CAP_PROP_" + setting.name.toUpperCase();
            try
            {
                int propID = Videoio.class.getDeclaredField(propName).getInt(null);
                camera.set(propID, setting.getValue());
            }
            catch (Exception e)
            {
                throw new VarException(setting.getVariable(), e.getMessage());
            }
        }
        
        // Read the first image to initialize the meta-data
        Mat mat = new Mat();
        camera.read(mat);
        IcyBufferedImage image = OpenCV.convertToIcy(mat);
        
        // Create the sequence
        final Sequence s = new Sequence("Live webcam", image);
        Icy.getMainInterface().addSequence(s);
        
        // don't update the channel bounds on update
        image.setAutoUpdateChannelBounds(false);
        s.setAutoUpdateChannelBounds(false);
        
        // Closing the sequence should stop the camera
        final Thread thread = Thread.currentThread();
        s.addListener(new SequenceAdapter()
        {
            @Override
            public void sequenceClosed(Sequence sequence)
            {
                s.removeListener(this);
                thread.interrupt();
            }
        });
        
        FPSMeter fps = new FPSMeter();
        
        try
        {
            while (camera.read(mat) && !Thread.currentThread().isInterrupted())
            {
                OpenCV.convertToIcy(mat, image);
                image.dataChanged();
                fps.update();
                getUI().setProgressBarMessage("Acquiring at " + fps.getFPS() + "fps");
            }
        }
        catch (Exception e)
        {
        
        }
        finally
        {
            camera.release();
        }
    }
    
    @Override
    public void clean()
    {
        for (EzVarInteger setting : parameters)
            setting.removeVarChangeListener(this);
        parameters.clear();
    }
    
    @Override
    public void variableChanged(EzVar<Integer> source, Integer newValue)
    {
        if (camera == null) return;
        
        String propName = "CAP_PROP_" + source.name.toUpperCase();
        try
        {
            int propID = Videoio.class.getDeclaredField(propName).getInt(null);
            camera.set(propID, newValue);
        }
        catch (Exception e)
        {
            throw new VarException(source.getVariable(), e.getMessage());
        }
    }
    
}
